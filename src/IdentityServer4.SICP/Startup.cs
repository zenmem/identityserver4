﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using IdentityServer4.SICP.Data;
using IdentityServer4.SICP.Models;
using IdentityServer4.SICP.Services;
using System.Reflection;
using IdentityServer4.EntityFramework;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Extensions;
using IdentityServer4.EntityFramework.Mappers;
using System.Linq;
using IdentityServer4.SICP.Configuration;
using System;
using Npgsql;

namespace IdentityServer4.SICP
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets();

                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            String ConnectionString;
            if (Environment.GetEnvironmentVariable("psql")!=String.Empty) {
                ConnectionString = "Host=" + Environment.GetEnvironmentVariable("IdentityServer4.SICP_DB_HOST")
                        + ";Username=" + Environment.GetEnvironmentVariable("IdentityServer4.SICP_DB_USER")
                        + ";Password=" + Environment.GetEnvironmentVariable("IdentityServer4.SICP_DB_PASSWORD")
                        + ";Database=" + Environment.GetEnvironmentVariable("IdentityServer4.SICP_DB_DB");

                using (var conn = new NpgsqlConnection(ConnectionString))
                {
                    bool opened = false;
                    int failed_attempts = 0;
                    while (!opened && failed_attempts < 100)
                    {
                        try
                        {
                            conn.Open();
                            opened = true;
                            conn.Close();
                        }
                        catch (Exception e)
                        {
                            opened = false;
                            failed_attempts++;
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    if(failed_attempts >= 100)
                    {
                        throw new Exception("Connection to PostgreSQL Database "+ Environment.GetEnvironmentVariable("IdentityServer4.SICP_DB_HOST") + "could not be opened");
                    }                    
                }

                services.AddDbContext<ApplicationDbContext>(options => 
                    options.UseNpgsql(ConnectionString));
            }
            else
            {
                ConnectionString = "Filename=./IdentityServer4.db";
                services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(ConnectionString));
            }



            //services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(""));

            //services.AddDbContext<ApplicationDbContext>(options =>
            //    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc();

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();


            // Add IdentityServer
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;


            if (Environment.GetEnvironmentVariable("psql") != String.Empty)
            {
                services.AddDeveloperIdentityServer()
                    .SetTemporarySigningCredential()

                    .AddConfigurationStore(builder =>
                    builder.UseNpgsql(ConnectionString, options =>
                    options.MigrationsAssembly(migrationsAssembly)))

                    .AddOperationalStore(builder =>
                    builder.UseNpgsql(ConnectionString, options =>
                    options.MigrationsAssembly(migrationsAssembly)))

                    .AddAspNetIdentity<ApplicationUser>();
            }
            else
            {
                services.AddDeveloperIdentityServer()
                    .SetTemporarySigningCredential()

                    .AddConfigurationStore(builder =>
                    builder.UseSqlite(ConnectionString, options =>
                    options.MigrationsAssembly(migrationsAssembly)))

                    .AddOperationalStore(builder =>
                    builder.UseSqlite(ConnectionString, options =>
                    options.MigrationsAssembly(migrationsAssembly)))

                    .AddAspNetIdentity<ApplicationUser>();
            }

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();

            // Setup Databases
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetService<ConfigurationDbContext>().Database.Migrate();
                serviceScope.ServiceProvider.GetService<PersistedGrantDbContext>().Database.Migrate();
                serviceScope.ServiceProvider.GetService<ApplicationDbContext>().Database.Migrate();
                //EnsureSeedData(serviceScope.ServiceProvider.GetService<ConfigurationDbContext>());

                //var dbContextOptions = app.ApplicationServices.GetRequiredService<DbContextOptions<PersistedGrantDbContext>>();
                var options = serviceScope.ServiceProvider.GetService<DbContextOptions<PersistedGrantDbContext>>();
                //var tokenCleanup = new TokenCleanup(options);
                //tokenCleanup.Start();
            }

            app.UseIdentity();

            // Add external authentication middleware below. To configure them please see http://go.microsoft.com/fwlink/?LinkID=532715

            // Adds IdentityServer
            app.UseIdentityServer();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        //Initialize Database for IdentityServer4.EntityFramework
        private static void EnsureSeedData(ConfigurationDbContext context)
        {
            if (!context.Clients.Any())
            {
                foreach (var client in Clients.Get().ToList())
                {
                    context.Clients.Add(client.ToEntity());
                }
                context.SaveChanges();
            }

            if (!context.Scopes.Any())
            {
                foreach (var client in Scopes.Get().ToList())
                {
                    context.Scopes.Add(client.ToEntity());
                }
                context.SaveChanges();
            }
        }
    }
}
